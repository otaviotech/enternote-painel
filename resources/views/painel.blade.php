<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Painel</title>
	
	<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:100,300,400,600,700" rel="stylesheet">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
  </head>
  <body>
  	<div class="container">
  	
  		<div class="row">
			<img src="img/enternote-logo.png" alt="EnterNote Logo" class="center-block" style="max-width: 50%">
	  	</div>

	  	<div class="row">
	    	<h1 class="text-center" style="font-size: 5em;">Painel</h1>
	  	</div>
	  	<br>
	  	<br>
	  	<div class="row">
	    	<h1 class="text-center" style="font-size: 13em; transition: 0.4s;">Retirada: <span id="ultima-os" style="font-weight: bold"><span></h1>
	  	</div>

	  	<div class="row">
	    	<h1 class="text-center">Anterior: <span id="anterior-1"></span></h1>
	  	</div>

	  	<div class="row">
	    	<h1 class="text-center">Anterior: <span id="anterior-2"></span></h1>
	  	</div>

	  	
		{{-- <audio class="hidden" id="notification">
		  <source src="http://www.thesoundarchive.com/email/tarzan-screaming.mp3" type="audio/mpeg">
		</audio> --}}
		<audio src="http://www.thesoundarchive.com/email/tarzan-screaming.mp3" id="notification"></audio>


  	</div>



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="{{ asset('js/app.js') }}"></script>
  </body>
</html>