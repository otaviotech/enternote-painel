<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Controle do painel</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

  </head>
  <body>
  	<div class="container">
  	
    <div class="row">
      <img src="img/enternote-logo.png" alt="EnterNote Logo" class="center-block" style="max-width: 50%">
      </div>

      <div class="row">
        <h1 class="text-center" style="font-size: 5em;">Controle do Painel</h1>
      </div>
      <br>
      <br>

	  	<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<input type="text" class="form-control" style="border-radius: 0px; background-color: transparent; height: 50px; font-size: 2em; color: #FFF;" id="os" placeholder="Número da OS">
          <br />
        <button class="btn btn-success btn-block" style="border-radius: 0px; height: 50px; font-size: 2em;" id="btn-solicitar-os">Solicitar!</button>
			</div>	  	
	  	</div>
		


  	</div>



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="{{ asset('js/app.js') }}"></script>
  </body>
</html>