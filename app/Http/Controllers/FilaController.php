<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FilaController extends Controller
{
    public function getFilaJSON()
    {
    	return \File::get('fila.json');
    }

    public function getFilaJSONtoArray()
    {
    	return json_decode(\File::get('fila.json'));
    }

    public function addItemFila(Request $request)
    {
    	$fila = $this->getFilaJSONtoArray();

    	$newItem =  [
    					'numero_os' 	=> $request->numero_os,
    					'funcionario' 	=> 'funcionario',//$request->funcionario,
    					'prateleira'	=> 'prateleira',//$request->prateleira,
    					'ordem' 		=> 'ordem'//$request->ordem
    				];

    	$fila[] = $newItem;

	// $file = \File::get('fila.json');
    \File::put('fila.json', '');
    \File::put('fila.json', json_encode($fila));

    // print_r(\File::get('fila.json'));

    return \File::get('fila.json');

    }



}
